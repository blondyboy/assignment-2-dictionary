%include "lib.inc"


global find_word


	
section .text


	;; rdi - указатель на нуль-терм. строку , rsi - указатель на начало словаря
find_word:
	.loop:
		push rsi
		push rdi
		add rsi, 8
		call string_equals
		pop rdi
		pop rsi
		cmp rax, 1		
		je .true
		mov rsi, [rsi] 	;проверяем указатель элемента словаря
		cmp rsi, 0
		je .false
		jmp .loop

	.true:
		mov rax, rsi
		ret
	.false:
		xor rax, rax
		ret
	
