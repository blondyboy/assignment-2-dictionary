ASM=nasm
FLAGS=-felf64
LD=ld

program: main.o dict.o lib.o
	ld -o $@ $^

%.o: %.asm
	$(ASM) $(FLAGS) -o $@ $<

main.o: main.asm words.inc lib.inc
	$(ASM) $(FLAGS) -o $@ $<

.PHONY: clean
clean:
	rm *.o program
