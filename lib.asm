global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

section .text

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte [rdi + rax], 0
        je .exit
        inc rax
        jmp .loop
    .exit:
        ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    push rdi
    call string_length
    mov rdx, rax; кладем в rdx длину строки
    pop rsi; кладем указатель
    mov rdi, 1
    mov rax, 1
    syscall
ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    pop rdi
    mov rdx, 1
    mov rdi, 1
    mov rax, 1
    syscall
ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, 0xA
    call print_char
    ret


; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
   xor rax, rax
   xor rbx, rbx
   mov rax, rdi
   mov r10, 10

   .loop:
       xor rdx, rdx
       div r10
       inc rbx
       push rdx
       cmp rax, 0
       jne .loop

   .print_digit:
       pop rdx
       add rdx, 0x30
       mov rdi, rdx
       push rcx
       call print_char
       pop rcx
       dec rbx
       jnz .print_digit
       xor rbx, rbx
       ret


; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    xor rax, rax
    cmp rdi, 0
    jge .positive
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi

    .positive:
        call print_uint
    ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor r10, r10

    .loop:
        mov al, [rdi + r10]
        sub al, [rsi + r10]
        cmp al, 0
        jne .false
        cmp byte [rdi + r10], 0
        je .true
        inc r10
        jmp .loop

    .false:
        xor rax, rax
        ret

    .true:
        xor rax, rax
        inc rax
        ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    push rax
    mov rsi, rsp
    xor rdi, rdi
    mov rdx, 1
    syscall
    pop rax
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    xor r10, r10

    .loop:
        push rdi
        push rsi
        push r10
        call read_char
        pop r10
        pop rsi
        pop rdi
        cmp rax, 0x20
        je .check_space
        cmp rax, 0x9
        je .check_space
        cmp rax, 0xA
        je .check_space
        cmp rax, 0x0
        je .finish
        mov [rdi+r10], rax
        inc r10
        cmp r10, rsi
        jl .loop
        mov rax, 0
        ret

    .check_space:
        and r10, r10
        jz .loop

    .finish:
        mov byte[rdi+r10], 0
        mov rdx, r10
        mov rax, rdi
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    push r10
    push rbx
    mov r10, 10
    xor rcx, rcx
    xor rbx, rbx
    xor rdx, rdx

    .loop:
      mov bl, [rdi + rcx]
      cmp rbx, '0'
      jl .finish
      cmp rbx, '9'
      jg .finish
      sub rbx, '0'
      mul r10
      add rax, rbx
      inc rcx
      jmp .loop

    .finish:
      mov rdx, rcx
      pop rbx
      pop r10
      ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rbx, rbx
    push rbx
    mov bl, [rdi]
    cmp bl, '-'
    jne .positive
    inc rdi

    .positive:
      call parse_uint
      cmp bl, '-'
      jne .finish
      neg rax
      inc rdx

    .finish:
      pop rbx
      ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    push rbx

    .loop:
        mov bl, byte[rdi+rax]
        mov byte[rsi+rax], bl
        dec rdx
        js .overflow
        cmp byte[rdi+rax], 0
        inc rax
        jne .loop
        jmp .finish

    .overflow:
        mov rax, 0

    .finish:
        pop rbx
        ret
