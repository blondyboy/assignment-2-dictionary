	%define BUFF_SIZE 255



	global _start
	extern find_word
	
section .data
	no_such_element_exeption: db 'Value not found', 0
	overflow_exeption: db "Value can't be more than 255 characters", 0

section .rodata
	%include "words.inc"
        %include "lib.inc"

	
section .bss
	buffer:	 resb BUFF_SIZE
section .text	
_start:	
	mov rdi, buffer
	mov rsi, BUFF_SIZE
	call read_word

	cmp rax, 0
	je .overflow

	mov rdi, buffer
	mov rsi, pointer
	call find_word

	cmp rax, 0
	je .no_such
	add rax, 8
	mov rdi, rax
	call string_length
	add rdi, rax
	inc rdi
	call print_string
	call exit

	.overflow:
	mov rdi, overflow_exeption
	call print_string
	call print_newline
	call exit

	.no_such:
	mov rdi, no_such_element_exeption
	call print_string
	call print_newline
	call exit

	
